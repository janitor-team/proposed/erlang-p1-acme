Source: erlang-p1-acme
Maintainer: Ejabberd Packaging Team <ejabberd@packages.debian.org>
Uploaders: Philipp Huebner <debalance@debian.org>
Section: libs
Priority: optional
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-rebar,
               erlang-base (>= 1:19.2),
               erlang-base64url (>= 1.0),
               erlang-crypto,
               erlang-eunit,
               erlang-idna (>= 6.0.0),
               erlang-jiffy (>= 1.0.5),
               erlang-jose (>= 1.9.0),
               erlang-p1-utils (>= 1.0.21),
               erlang-p1-yconf (>= 1.0.10),
               erlang-p1-yaml (>= 1.0.30),
               erlang-syntax-tools,
               erlang-unicode-util-compat (>= 0.3.1)
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/ejabberd-packaging-team/erlang-p1-acme
Vcs-Git: https://salsa.debian.org/ejabberd-packaging-team/erlang-p1-acme.git
Homepage: https://github.com/processone/p1_acme

Package: erlang-p1-acme
Architecture: any
Depends: ${shlibs:Depends},
         ${misc:Depends},
         erlang-base (>= 1:19.2) | ${erlang-abi:Depends},
         ${erlang:Depends},
         erlang-base64url (>= 1.0),
         erlang-idna (>= 6.0.0),
         erlang-jiffy (>= 1.0.5),
         erlang-jose (>= 1.9.0),
         erlang-p1-utils (>= 1.0.21),
         erlang-p1-yconf (>= 1.0.10),
         erlang-p1-yaml (>= 1.0.30),
         erlang-unicode-util-compat (>= 0.3.1)
Multi-Arch: allowed
Description: ACME client library for Erlang
 This Erlang library provides an ACME client implementing RFC 8555.
 It was written for ejabberd which still uses it. It was split off into its
 own project to follow Erlang/OTP guidelines.
